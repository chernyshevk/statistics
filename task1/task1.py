#!/usr/bin/python3
import scipy.stats as stat
import matplotlib.pyplot as plt
import numpy as np


def moment_gen(N,K,dist): # генерация выборки и нахождение выборочных моментов
    
    f = dist()
    M = np.zeros(K)
    
    for i in range(N):
        val = f.rvs()
        mom = val
        for j in range(K):
            M[j] += mom
            mom *= val
    
    M /= N
    return M

    
def estimate_uni(N,K):  # вычисление оценок равномерного распределения 
                        # для одной выборки
    
    M = moment_gen(N, K, stat.uniform)

    for j in range(K):
        M[j] = ((j+2)*M[j])**(1/(j+1))
        
    return M

    
def estimate_exp(N,K):  # вычисление оценок экспоненциального распределения 
                        # для одной выборки 
 
    M = moment_gen(N, K, stat.expon)
    
    fact = 1
    for j in range(K):
        M[j] = (M[j]/fact)**(1/(j+1))
        fact *= (j+2)
        
    return M


def sol(K,N,P,theta,est): # нахождение СКО по найденным оценкам

    res = np.zeros(K)
    
    for _ in range(P):
        M = est(N,K)
        res = res + M*(M-2*theta)
    
    res /= P
    res += theta**2

    return res

    
def plot(res,K,title):   # построение графика СКО
    
    plt.plot(range(1,K+1),res)
    plt.title(title)
    plt.xlabel("Power of moment")
    plt.ylabel("Error")
    plt.grid(True)
    plt.show()

    
if __name__=="__main__":

    K = 20    # максимальная степень пробной функции
    N = 100   # количество элементов в одной выборке
    P = 1000  # количество выборок
    theta = 1.0    # параметр распределения
    
    res = sol(K, N, P, theta, estimate_uni)
    plot(res, K, "Uniform [0;1]")
    
    res = sol(K, N, P, theta, estimate_exp)
    plot(res, K, "Exp(1)")