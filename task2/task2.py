#!/usr/bin/python3
import scipy.stats as stat
import matplotlib.pyplot as plt
import numpy as np


def f_gen(M,S):  # Генерация статистики для интервалов первого типа
    
    for _ in range(M):
        val = stat.norm.rvs()
        S += val**2
    
    return S


def first(M,S,points,i,g):
    
    N = M*(i+1)

    high = stat.chi2.ppf((1+g)/2,N)  # Квантили распределения Хи-квадрат
    low = stat.chi2.ppf((1-g)/2,N)   # с N степенями свободы
    
    points[i,0] = S/high
    points[i,1] = S/low
    
    return S


def s_gen(M,S):   # Генерация статистики для интервалов второго типа
    
    for _ in range(M):
        val = stat.norm.rvs()
        S += val
    
    return S

     
def second(M,S,points,i,g):
    
    N = M*(i+1)

    low = stat.chi2.ppf((1-g)/2,1)   # Квантили распределения Хи-квадрат
    high = stat.chi2.ppf((1+g)/2,1)  # с 1 степенью свободы
    
    points[i,0] = S**2/(high*N)
    points[i,1] = S**2/(low*N)

    
def sol(K,M,gamma,gen,conf_int):

    S = 0
    sz = gamma.shape[0]         
    intervals = np.zeros((sz,K,2))
    
    for i in range(K):        # по каждому инкременту объема выборки
        S = gen(M,S)
        for j in range(sz):   # по всем уровням
            conf_int(M,S,intervals[j],i,gamma[j])   
    
    return intervals
    

if __name__=="__main__":

    K = 500    # число увеличений объема выборки 
    M = 100    # инкремент выборки 
  
    gamma = np.array([0.8,0.95])  # уровни доверительных интервалов
   
    f = sol(K,M,gamma,f_gen,first)
    s = sol(K,M,gamma,s_gen,second)             
    
    samples = range(M,M*K+1,M)
    
    plt.plot(samples,f[0,:,0],'ro',label="gamma = 0.8")
    plt.plot(samples,f[0,:,1],'ro')
    
    plt.plot(samples,f[1,:,0],'bo',label="gamma = 0.95")
    plt.plot(samples,f[1,:,1],'bo')
    
    plt.title('1. Confidence interval endpoints')
    plt.xlabel("Number of samples")
    plt.ylabel("End points of intervals")
    plt.grid(True)
    plt.legend()
    plt.show()
